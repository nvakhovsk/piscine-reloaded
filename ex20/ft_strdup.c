/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 14:26:54 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/07 12:54:34 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	char			*s;
	unsigned long	i;
	unsigned long	len;

	len = 0;
	while (*(src + len))
		len++;
	i = 0;
	s = malloc(sizeof(char) * len + 1);
	if (s)
	{
		while (i++ < len)
			*(s + (i - 1)) = *(src + (i - 1));
		*(s + (i - 1)) = 0;
	}
	return (s);
}
