/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 18:41:45 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/07 12:54:40 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int *arr;
	int i;
	int size;

	i = 0;
	if (max <= min)
		return (0);
	else
	{
		size = max - min;
		arr = (int *)malloc(sizeof(int) * size);
		while (min < max)
			arr[i++] = min++;
		return (arr);
	}
	return (0);
}
