/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/29 23:17:55 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/07 12:53:57 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int rt;
	int neg;

	if (nb < 0)
	{
		neg = -1;
		nb = -nb;
	}
	else
		neg = 1;
	if (nb > 12)
		return (0);
	rt = 1;
	while (nb > 1)
	{
		rt *= nb;
		nb--;
	}
	return (rt * neg);
}
