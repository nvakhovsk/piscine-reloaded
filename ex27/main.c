/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:31:51 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/07 12:55:37 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>

#define BUFF 2048

int		ft_strlen(char *str)
{
	int res;

	res = 0;
	if (str)
		while (str[res])
			res++;
	return (res);
}

int		ft_putstr(int fd, char *str)
{
	write(fd, str, ft_strlen(str));
	if (fd == 2)
		return (1);
	return (0);
}

int		display_file(char *filename)
{
	char	buf[BUFF + 1];
	int		fd;
	int		ret;

	if ((fd = open(filename, O_RDONLY)) == -1)
		return (ft_putstr(2, "Error opening file.\n"));
	while ((ret = read(fd, buf, BUFF)))
	{
		if (ret == -1)
		{
			ft_putstr(2, "Error reading file.\n");
			break ;
		}
		buf[ret] = 0;
		ft_putstr(1, buf);
	}
	if (close(fd) == -1)
		return (ft_putstr(2, "Error closing file.\n"));
	return (0);
}

int		main(int argc, char **argv)
{
	if (argc == 1)
		return (ft_putstr(2, "File name missing.\n"));
	if (argc > 2)
		return (ft_putstr(2, "Too many arguments.\n"));
	return (display_file(argv[1]));
}
