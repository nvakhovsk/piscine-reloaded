/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/29 12:39:10 by nmatushe          #+#    #+#             */
/*   Updated: 2017/11/07 12:54:09 by nmatushe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int x;
	int xx;

	if (nb == 1)
		return (1);
	else if (nb < 4)
		return (0);
	else if (nb < 100)
		x = 7;
	else if (nb < 1000)
		x = 22;
	else if (nb < 10000)
		x = 70;
	else
		x = 225;
	while (x * x != nb)
	{
		xx = (x + nb / x) / 2;
		if (xx == x)
			return (0);
		else
			x = xx;
	}
	return (x);
}
